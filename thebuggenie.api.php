<?php

/**
 * @file
 * Hooks provided by the "The Bug Genie API" module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Change project list before return thebuggenie_list_projects() function.
 *
 * @param array $projects
 *   An associative array, where key is the project key and the
 *   value is the project name.
 */
function hook_thebuggenie_list_projects_alter(&$projects) {
  $projects['foo'] = 'Bar';
}

/**
 * Change issue list before return thebuggenie_list_issues() function.
 *
 * @param array $issues
 *   An associative array, where key is the issue identify key and the
 *   value is keyed array, with the issues data.
 * @param array $filter
 *   An associative array, with filter options.
 */
function hook_thebuggenie_list_issues_alter(&$issues, $filter) {
  foreach ($issues as $key => $issue) {
    $issues[$key]['new_item'] = 'foobar';
  }
}

/**
 * Change issue types list.
 *
 * Call before return thebuggenie_list_issuetypes() function.
 *
 * @param array $types
 *   Simple issue types list.
 */
function hook_thebuggenie_list_issuetypes_alter(&$types) {
  $types['foo'] = 'Bar';
}

/**
 * Change issue fields list.
 *
 * Call before return thebuggenie_list_issuefields() function.
 *
 * @param array $fields
 *   Simple issue fields list.
 * @param array $filter
 *   An associative array, with filter options.
 */
function hook_thebuggenie_list_issuefields_alter(&$fields, $filter) {
  $fields[] = 'foobar';
}

/**
 * Change issue list.
 *
 * Call before return thebuggenie_list_issues() function.
 *
 * @param array $values
 *   An associative array, with type, description, and other informations.
 * @param array $filter
 *   An associative array, with filter options.
 */
function hook_thebuggenie_list_fieldvalues_alter(&$values, $filter) {
  // Remove description data.
  unset($values['description']);
}

/**
 * Change issue before creating with thebuggenie_create_issue() function.
 *
 * @param array $issue
 *   An associative array, with issue datas.
 */
function hook_thebuggenie_create_issue_alter(&$issue) {
  $issue['title'] .= ' - new issue';
}

/**
 * @} End of "addtogroup hooks".
 */
